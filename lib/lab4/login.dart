import 'package:flutter/material.dart';
import 'home.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset('assets/images/jata.png'),
                TextFormField(
                  decoration: InputDecoration(label: Text('No K/P')),
                ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(label: Text('Password')),
                ),
                const SizedBox(
                  height: 15,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => HomePage(nama: 'Farid')));
                  },
                  child: const Text('Log Masuk'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
