import 'package:flutter/material.dart';

import 'hubungi.dart';

class HelloPage extends StatelessWidget {
  const HelloPage({
    super.key,
    required this.nama,
  });

  final String nama;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 25),
          Text('Hai $nama', style: Theme.of(context).textTheme.headlineMedium),
          ElevatedButton(onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => HubungiPage())
            );
          }, child: Text('Hubungi Kami'))
        ],
      ),
    );
  }
}