import 'hello.dart';
import 'senarai.dart';

import 'login.dart';
import 'hubungi.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({required this.nama, super.key});
  String nama;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _selectedIndex = 0;
  final List<Widget> _pages = [
    HelloPage(nama: 'Farid'),
    SenaraiPage(),
    HubungiPage(),
  ];

  onIconTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Saya'),
        actions: [
        IconButton(onPressed: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
        }, icon: Icon(Icons.logout))
      ]),
      body: _pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.list), label: 'Senarai'),
          BottomNavigationBarItem(icon: Icon(Icons.contact_page), label: 'Hubungi'),
        ],
        currentIndex: _selectedIndex,
        onTap: onIconTap,
      ),
    );
  }
}