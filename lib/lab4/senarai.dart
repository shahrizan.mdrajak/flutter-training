import 'package:flutter/material.dart';

class SenaraiPage extends StatefulWidget {
  const SenaraiPage({super.key});

  @override
  State<SenaraiPage> createState() => _SenaraiPageState();
}

class _SenaraiPageState extends State<SenaraiPage> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(8),
      itemCount: 20,
      itemBuilder: (context, position) {
        return ListTile(
          leading: CircleAvatar(),
          title: Text(
            "Pengguna " + position.toString(),
          ),
          trailing: Icon(Icons.arrow_forward),
          subtitle: Text('abc@gmail.com'),
        );
      },
    );
  }
}
