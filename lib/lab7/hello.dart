import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Model/User.dart';
import 'hubungi.dart';

class HelloPage extends StatefulWidget {
  const HelloPage({
    super.key,
  });

  @override
  State<HelloPage> createState() => _HelloPageState();
}

class _HelloPageState extends State<HelloPage> {
  User pengguna = User();
  ImageProvider gambar = AssetImage("assets/images/people.jpg");

  double _top = 0;
  double _left = 0;

  @override
  void initState() {
    super.initState();
    loadSharedPref();
  }

  void loadSharedPref() async {
    final pref = await SharedPreferences.getInstance();
    setState(() {
      print(pref.getString('user'));
      pengguna = pengguna.postFromJson(pref.getString('user')!);
      gambar = NetworkImage(pengguna.image!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: _top,
          left: _left,
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                _top = max(0, _top + details.delta.dy);
                _left = max(0, _left + details.delta.dx);
              });
            },
            child: CircleAvatar(
              foregroundImage: gambar,
              radius: 50,
            ),
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 25),
              Text('Hai ${pengguna.firstName} ${pengguna.lastName}',
                  style: Theme.of(context).textTheme.headlineMedium),
            ],
          ),
        ),
      ],
    );
  }
}
