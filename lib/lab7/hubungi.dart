import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HubungiPage extends StatefulWidget {
  const HubungiPage({super.key});

  @override
  State<HubungiPage> createState() => _HubungiPageState();
}

class _HubungiPageState extends State<HubungiPage>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation1;
  late Animation<Offset> _animation2;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3),
    )..repeat(reverse: true);

    // Fade Transition
    _animation1 = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    );

    // Slide Transition
    _animation2 = Tween<Offset>(
      begin: const Offset(-0.3, 0.0),
      end: const Offset(0.3, 0.0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeInOut,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FadeTransition(
              opacity: _animation1,
              child: Image.asset('assets/images/jata.png'),
            ),
            SlideTransition(
              position: _animation2,
              child: Text(
                'Hubungi Kami',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              "Unit Pemodenan Tadbiran dan Perancangan Pengurusan Malaysia, Kompleks Setia Perdana\nPutrajaya",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'tel',
                        path: '049738800',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.call)),
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'mailto',
                        path: 'webmaster@mampu.gov.my',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.email)),
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'https',
                        path: 'www.mampu.gov.my/',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.web)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw Exception('Could not launch');
    }
  }
}
