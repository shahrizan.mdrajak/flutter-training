import 'dart:convert';
import 'package:flutter/material.dart';

class User {
  String? id;
  String? firstName;
  String? lastName;
  String? email;
  String? username;
  String? password;
  String? image;
  String? token;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.username,
    this.password,
    this.image,
    this.token,
  });

  User postFromJson(String str) {
    final jsonData = json.decode(str);
    // print(jsonData.toString());
    return User.fromJson(jsonData);
  }

  String postToJson(User data) {
    final dyn = data.toJson();
    return json.encode(dyn);
  }

  List<User> allPostsFromJson(String str) {
    final jsonData = json.decode(str);
    // print( jsonData["articles"][0]["urlTogambar"]);
    // print("response" + jsonData.toString());
    print('Importing data from JSON...');

    List<User> test;
    Iterable list = jsonData['users'];
    test = list.map((e) => User.fromJson(e)).toList();
    // test.forEach((element) {
    //   print(element.dateP);
    // });
    print('Done');
    return test;
  }

  String allPostsToJson(List<User> data) {
    final dyn = List<dynamic>.from(data.map((x) => x.toJson()));
    return json.encode(dyn);
  }

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"].toString(),
    firstName: json["firstName"],
    lastName: json["lastName"],
    email: json["email"],
    username: json["username"],
    password: json["password"],
    image: json["image"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "email": email,
    "username": username,
    "password": password,
    "image": image,
    "token": token,
  };
}