import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Shared/services.dart';
import 'home.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String username = 'kminchelle';
  String password = '0lelplR';
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset('assets/images/jata.png'),
                TextFormField(
                    initialValue: 'kminchelle',
                    decoration: InputDecoration(label: Text('No K/P')),
                    onChanged: (value) {
                      setState(() {
                        username = value;
                      });
                    }),
                TextFormField(
                    initialValue: '0lelplR',
                    obscureText: true,
                    decoration: InputDecoration(label: Text('Password')),
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    }),
                const SizedBox(
                  height: 15,
                ),
                Visibility(
                    visible: isLoading, child: LinearProgressIndicator()),
                const SizedBox(
                  height: 15,
                ),
                // ElevatedButton(onPressed: () {
                //   Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage(nama: 'Farid')));
                // }, child: const Text('Log Masuk'),),

                ElevatedButton(
                  onPressed: () async {
                    login();
                  },
                  child: const Text('Log Masuk'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void login() async {
    debugPrint('Logging in...');
    setState(() {
      isLoading = true;
    });

    var result = await loginRequest(username, password);
    if (result['id'] == null) {
      var snackBar = SnackBar(content: Text('Kata laluan tidak tepat'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      var nama = result['firstName'] + ' ' + result['lastName'];
      debugPrint("Nama: $nama");

      //save value
      final pref = await SharedPreferences.getInstance();
      pref.setString('user', json.encode(result));

      //route
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    }

    //done
    setState(() {
      isLoading = false;
    });
  }
}
