import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HubungiPage extends StatelessWidget {
  const HubungiPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/jata.png'),
            Text(
              'Hubungi Kami',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              "Unit Pemodenan Tadbiran dan Perancangan Pengurusan Malaysia, Kompleks Setia Perdana\nPutrajaya",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'tel',
                        path: '049738800',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.call)),
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'mailto',
                        path: 'webmaster@mampu.gov.my',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.email)),
                IconButton(
                    onPressed: () {
                      final Uri launchUri = Uri(
                        scheme: 'https',
                        path: 'www.mampu.gov.my/',
                      );
                      _launchUrl(launchUri);
                    },
                    icon: Icon(Icons.web)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw Exception('Could not launch');
    }
  }
}
