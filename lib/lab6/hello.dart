import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Model/User.dart';
import 'hubungi.dart';

class HelloPage extends StatefulWidget {
  const HelloPage({
    super.key,
  });

  @override
  State<HelloPage> createState() => _HelloPageState();
}

class _HelloPageState extends State<HelloPage> {

  User pengguna = User();
  ImageProvider gambar =  AssetImage("assets/images/people.jpg");

  @override
  void initState() {
    super.initState();
    loadSharedPref();
  }

  void loadSharedPref() async {
    final pref = await SharedPreferences.getInstance();
    setState(() {
      print(pref.getString('user'));
      pengguna = pengguna.postFromJson(pref.getString('user')!);
      gambar = NetworkImage(pengguna.image!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            foregroundImage: gambar,
            radius: 50,
          ),
          SizedBox(height: 25),
          Text('Hai ${pengguna.firstName} ${pengguna.lastName}', style: Theme.of(context).textTheme.headlineMedium),
        ],
      ),
    );
  }
}