import 'package:flutter/material.dart';

class HubungiPage extends StatelessWidget {
  const HubungiPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Hubungi Kami')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/jata.png'),
            Text(
              'Hubungi Kami',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              "Unit Pemodenan Tadbiran dan Perancangan Pengurusan Malaysia, Kompleks Setia Perdana\nPutrajaya",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(onPressed: () {}, icon: Icon(Icons.call)),
                IconButton(onPressed: () {}, icon: Icon(Icons.email)),
                IconButton(onPressed: () {}, icon: Icon(Icons.web)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
