import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../Model/User.dart';

String baseURL = 'https://dummyjson.com';

Map<String, String> headers = {
  'Content-type': 'application/json',
};

Future<Map> loginRequest(String username, String password) async {

  var url = '/auth/login';
  Uri apiUrl = Uri.parse(baseURL + url);
  debugPrint('Call API: $url');

  Map<String, String> headers = {
    'Content-type': 'application/json',
  };

  String body = json.encode({
    'username': username,
    'password': password,
  });

  http.Response response =
      await http.post(apiUrl, body: body, headers: headers);

  debugPrint('Username: $username');
  debugPrint('Password: $password');
  debugPrint('Response Code: ${response.statusCode.toString()}');
  if (response.statusCode == 200) {
    debugPrint('\n');
    debugPrint(response.body);
    debugPrint('done');
    debugPrint('\n\n');

    return json.decode(response.body); // returns a Map type
  } 
  else if (response.statusCode == 400) {
    return json.decode('{"result":"false"}');
  }
  else {
    debugPrint(response.body);
    throw Exception('Error return response');
  }
}

Future<List<User>> getUsers() async {
  var url = baseURL + '/users';
  Uri apiUrl = Uri.parse(url);
  Map<String, String> headers = {
    'Content-type': 'application/json',
  };

  debugPrint('Call API: ' + url);
  http.Response response = await http.get(apiUrl, headers: headers);

  debugPrint(response.body);

  if (response.statusCode != 200) {
    throw Exception('Failed to load data from API');
  }
  return User().allPostsFromJson(response.body);
}
