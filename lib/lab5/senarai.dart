import 'package:flutter/material.dart';

import 'Model/User.dart';
import 'Shared/services.dart';

class SenaraiPage extends StatefulWidget {
  const SenaraiPage({super.key});

  @override
  State<SenaraiPage> createState() => _SenaraiPageState();
}

class _SenaraiPageState extends State<SenaraiPage> with AutomaticKeepAliveClientMixin<SenaraiPage> {

  @override
  bool get wantKeepAlive => true;

  List<User> senarai = [];
  ImageProvider gambar = AssetImage("assets/images/people.jpg");
  
  @override
  void initState() {
    super.initState();
    loadUsers();
  }

  void loadUsers() {
    getUsers().then((value) => {
      setState(() {
        senarai = value;
      })
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: (senarai.length > 0),
      replacement: const LinearProgressIndicator(),
      child: ListView.builder(
        padding: EdgeInsets.all(8),
            itemCount: senarai.length,
            itemBuilder: (context, position) {
              gambar = NetworkImage(senarai[position].image!);
    
              return ListTile(
                leading: CircleAvatar(
                  foregroundImage: gambar,
                ),
                title:
                  Text(
                    senarai[position].firstName!,
                  ),
                subtitle: Text(senarai[position].email!),
              );
            },
          ),
    );
  }
}